package es.ideotec.t16fling

import android.inputmethodservice.InputMethodService
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.children


class T16Fling : InputMethodService() {

    private val ENTER_KEY = 44
    private val SHIFT_KEY = 24
    private val CHAR_KEYS = intArrayOf(12, 13, 21, 22, 23, 31, 32, 33)

    private var inputMethodManager: InputMethodManager? = null
    private var languageTag = "en"
    private var keyboard: View? = null

    private var multiline = false

    private var shifted = false
    private var capsLocked = false

    fun setShift(value: Boolean? = null) {
        if (value != null) {
            shifted = value
            updateShiftKey()
            updateCharKeys()
        } else if (!capsLocked) {
            val capsModeMask = TextUtils.CAP_MODE_WORDS or TextUtils.CAP_MODE_SENTENCES
            val capsMode = this.currentInputConnection.getCursorCapsMode(capsModeMask)
            if (capsMode > 0) setShift(true)
            else setShift(false)
        } else setShift(false)
    }

    fun toggleShift() {
        setShift(!shifted)
    }

    fun setCapsLock(value: Boolean) {
        capsLocked = value
        shifted = false
        updateShiftKey()
        updateCharKeys()
    }

    private fun updateEnterKey() {
        val enterKeyId = resources.getIdentifier("key$ENTER_KEY", "id", packageName)
        val enterMultiKeyId = resources.getIdentifier("key${ENTER_KEY}multi", "id", packageName)
        if (multiline) {
            keyboard?.findViewById<Key>(enterKeyId)?.visibility = View.GONE
            keyboard?.findViewById<Key>(enterMultiKeyId)?.visibility = View.VISIBLE
        } else {
            keyboard?.findViewById<Key>(enterKeyId)?.visibility = View.VISIBLE
            keyboard?.findViewById<Key>(enterMultiKeyId)?.visibility = View.GONE
        }
    }

    private fun updateShiftKey() {
        // Set the correct view for the shift key (4 options)
        val shiftKeyId = resources.getIdentifier("key$SHIFT_KEY", "id", packageName)
        val shiftedKeyId = resources.getIdentifier("key${SHIFT_KEY}shifted", "id", packageName)
        val capslockedKeyId = resources.getIdentifier("key${SHIFT_KEY}capslocked", "id", packageName)
        val shiftedCapslockedKeyId = resources.getIdentifier("key${SHIFT_KEY}shiftedcapslocked", "id", packageName)
        keyboard?.findViewById<Key>(shiftKeyId)?.visibility = View.GONE
        keyboard?.findViewById<Key>(shiftedKeyId)?.visibility = View.GONE
        keyboard?.findViewById<Key>(capslockedKeyId)?.visibility = View.GONE
        keyboard?.findViewById<Key>(shiftedCapslockedKeyId)?.visibility = View.GONE
        when {
            !shifted && !capsLocked -> keyboard?.findViewById<Key>(shiftKeyId)?.visibility = View.VISIBLE
            shifted && !capsLocked -> keyboard?.findViewById<Key>(shiftedKeyId)?.visibility = View.VISIBLE
            !shifted && capsLocked -> keyboard?.findViewById<Key>(capslockedKeyId)?.visibility = View.VISIBLE
            shifted && capsLocked -> keyboard?.findViewById<Key>(shiftedCapslockedKeyId)?.visibility = View.VISIBLE
        }
    }

    private fun updateCharKeys() {
        // Set the correct view for the keys that have an upperCase option
        for (key in CHAR_KEYS) {
            val ucKeyId = resources.getIdentifier("key${key}uc", "id", packageName)
            if (ucKeyId > 0) {
                val keyId = resources.getIdentifier("key$key", "id", packageName)
                if (shifted xor capsLocked) {
                    keyboard?.findViewById<Key>(ucKeyId)?.visibility = View.VISIBLE
                    keyboard?.findViewById<Key>(keyId)?.visibility = View.GONE
                } else {
                    keyboard?.findViewById<Key>(ucKeyId)?.visibility = View.GONE
                    keyboard?.findViewById<Key>(keyId)?.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onCreate() {
        super.onCreate()
        inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
    }

    override fun onCreateInputView(): View {
        languageTag = inputMethodManager?.currentInputMethodSubtype?.languageTag ?: "en"
        val layoutId = resources.getIdentifier("input_$languageTag", "layout", packageName)
        return layoutInflater.inflate(layoutId, null).apply {
            keyboard = this.findViewById<ConstraintLayout>(R.id.keyboard)
            for (child in (keyboard as ConstraintLayout?)?.children?: emptySequence()) {
                if (child is Key) child.setIME(this@T16Fling)
            }
        }
    }

    override fun onStartInput(info: EditorInfo?, restarting: Boolean) {
        super.onStartInput(info, restarting)

        // Set the shift state
        capsLocked = false
        shifted = false
        if (info?.initialCapsMode?:0 > 0) setShift(true)
        else setShift(false)

        // Set the enter key type
        multiline = false
        if (info?.inputType?.and(EditorInfo.TYPE_TEXT_FLAG_MULTI_LINE) ?: 0  != 0) {
            multiline = true
        }
        updateEnterKey()
    }

}