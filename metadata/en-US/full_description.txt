T16Fling is an Android keyboard with 16 keys in a 4x4 grid (similar to the AOSP numeric keypad) that works by flinging each key in the direction of the desired character.

Once installed you must enable the keyboard in your Android Settings (System -> Languages & input -> Virtual keyboard -> Manage keyboards).

Each key has one main character (shown in a larger and darker font on the left) and one or more secondary characters (shown in a smaller and lighter font on the right). Depending on the gesture you use on the key it will produce different characters:
- When tapped, a key will produce its main character.
- When flinged (swiped quickly in a direction) a key will produce the secondary character that is in that direcion.
- When long-pressed, new keys will appear with extra characters related to those on the main key. Tapping on any of these keys will produce the corresponding character.

For example, the second key in the first row (with a large "2" on the left and "a<sup>b</sup>c" on the right) will produce a "2" when tapped, an "a" when flinged left, a "b" when flinged up, and a "c" when flinged right. When long-pressed it will show new keys "á", "à", "â", "ä", "ã", and "ç".

There are some exceptions to these rules:
- The "0" key only has one secondary character, which is space. No matter which direction you fling it, it will always produce a whitespace.
- The shift key (⇪) has two special functions associated to its fling: when flinged up it will turn on Caps Lock, and when flinged down it will turn it off.
- The backspace key (⇦) and the enter key (➡) always do the same thing, no matter if you tap them or fling them.